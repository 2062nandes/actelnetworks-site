import '@fortawesome/fontawesome-free/js/all'
import './components'
import './libraries/particles'
import plugins from './plugins'
import 'alpinejs'

(() => {
  const video = document.getElementById('video')
  const video2 = document.getElementById('video2')
  const btn = document.getElementById('buttonPlay')
  let count = 0
  try {
    btn.addEventListener('click', () => {
      if (count < 4) {
        video.play()
        video2.play()
        count++
      }
    })
  } catch (error) {
    // console.log(error)
  }
  const links = document.querySelectorAll('.scroll-item')
  const sections = document.querySelectorAll('section')
  let aux = 0

  function changeLinkState() {
    let index = sections.length
    while(--index && window.scrollY + 180 < sections[index].offsetTop) {}
    try {
      if (index > aux) {
        links.forEach((link) => link.classList.remove('scroll-active'))
        links[index].classList.add('scroll-active')
        aux++
      } else if (index < aux) {
        links.forEach((link) => link.classList.remove('scroll-active'))
        links[index].classList.add('scroll-active')
        aux--
      }
    } catch (error) {}
  }
  try {
    changeLinkState()
    window.addEventListener('scroll', changeLinkState)
    window.addEventListener('resize', changeLinkState);
  } catch (error) {
   console.log('none class') 
  }
  
  return {
    plugins
  }
})()

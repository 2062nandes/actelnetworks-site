// import tinyslider from './tinyslider'
import { headroom, headroom2 } from './headroom'
import smoothscroll from './smoothscroll'
import aos from './aos'
// import gio from './gio'

// Load Plugins
export default {
  aos,
  // gio,
  headroom,
  headroom2,
  // tinyslider,
  smoothscroll
}

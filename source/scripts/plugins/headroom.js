import Headroom from 'headroom.js'
const header = document.getElementById('header')
const upBtn = document.getElementById('upBtn')
const options = {
  offset: 10,
  tolerance: 5,
  classes: {
    pinned: 'headroom--pinned',
    unpinned: 'headroom--unpinned',
    unfixed: 'headroom--unfixed',
    top: 'headroom--top',
    notTop: 'headroom--not-top',
    bottom: 'headroom--bottom',
    notBottom: 'headroom--not-bottom',
    initial: 'headroom'
  }
}
const headroom = new Headroom(header, options)
const headroom2 = new Headroom(upBtn, options)
headroom.init()
headroom2.init()

export default {
  headroom,
  headroom2
}

import * as GIO from 'giojs'

const configs = {
  'control': {
    'stats': false,
    'disableUnmentioned': true,
    'lightenMentioned': false,
    'inOnly': false,
    'outOnly': false,
    'initCountry': 'BO',
    'halo': true,
    'transparentBackground': false
    // 'autoRotation': true,
    // 'rotationRatio': -0.2
  },
  'color': {
    'surface': 1744048,
    'selected': 2141154,
    'in': 16777215,
    'out': 2141154,
    'halo': 2141154
  },
  'brightness': {
    'ocean': 1,
    'mentioned': 1,
    'related': 1
  }
}
const data = [
  {
    'e': 'US',
    'i': 'PE',
    'v': 100100012
  },
  {
    'e': 'US',
    'i': 'BR',
    'v': 100100012
  },
  {
    'e': 'CL',
    'i': 'BO',
    'v': 1001000
  },
  {
    'e': 'BO',
    'i': 'PY',
    'v': 1001000
  },
  {
    'e': 'AR',
    'i': 'BO',
    'v': 1001000
  },
  {
    'e': 'BO',
    'i': 'PE',
    'v': 1001000
  },
  {
    'e': 'BR',
    'i': 'BO',
    'v': 1001000
  },
  {
    'e': 'BO',
    'i': 'AR',
    'v': 1001000
  }
]
try {
  let container = document.getElementById('globeArea')
  let controller = new GIO.Controller(container, configs)
  controller.setTransparentBackground(true)
  controller.addData(data)
  controller.init()
} catch (error) {
  console.log('No pudo mostrarse el mapa global')
}

export default GIO

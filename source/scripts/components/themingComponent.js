export default window.themingColor = function () {
  return {
    themeMode: 'light',
    colorsTheme: null,
    head: null,
    style: null,
    changeTheme () {
      if (this.themeMode === 'light') {
        this.themeMode = 'dark'
        this.colorsTheme.innerHTML = this.darkTheme()
      } else if (this.themeMode === 'dark') {
        this.themeMode = 'light'
        this.colorsTheme.innerHTML = ''
      }
      localStorage.setItem('theme', this.colorsTheme.innerHTML)
    },
    initialize () {
      this.head = document.head || document.getElementsByTagName('head')[0]
      this.style = document.createElement('style')
      this.style.type = 'text/css'
      this.style.setAttribute('id', 'stateTheme')
      this.head.appendChild(this.style)
      this.colorsTheme = document.getElementById('stateTheme')
      if (localStorage.getItem('theme') !== '') {
        this.colorsTheme.innerHTML = localStorage.getItem('theme')
        if (this.colorsTheme.innerHTML !== '') {
          this.themeMode = 'dark'
        }
      }
    },
    darkTheme () {
      return `
      :root {
        --light-color: #F6F9FE;
        --light-HS: 218, 80%;
        --light-HSL: 218, 80%, 98%;
        --dark-color: #252D59;
        --dark-HS: 231, 41%;
        --dark-HSL: 231, 41%, 25%;
    
        --theme-color: var(--dark-color);
        --theme-050: hsl(var(--dark-HS), 15%);
        --theme-100: hsl(var(--dark-HS), 20%);
        --theme-150: hsl(var(--dark-HS), 25%);
        --theme-200: hsl(var(--dark-HS), 30%);
        --theme-250: hsl(var(--dark-HS), 35%);
        --theme-300: hsl(var(--dark-HS), 40%);
        --theme-350: hsl(var(--dark-HS), 45%);
        --theme-400: hsl(var(--dark-HS), 50%);
        --theme-450: hsl(var(--dark-HS), 55%);
        --theme-500: hsl(var(--dark-HS), 60%);
        --theme-550: hsl(var(--dark-HS), 65%);
        --theme-600: hsl(var(--dark-HS), 70%);
        --theme-650: hsl(var(--dark-HS), 75%);
        --theme-700: hsl(var(--dark-HS), 80%);
        --theme-750: hsl(var(--dark-HS), 85%);
        --theme-800: hsl(var(--dark-HS), 90%);
        --theme-850: hsl(var(--dark-HS), 95%);
        --theme-900: hsl(var(--dark-HS), 100%);
    
        --text-color: #fcfcfc;
        --text-HS: 0, 0%;
        --text-HSL: 0, 0%, 99%;
    
        --primary-color: #fcfcfc;
        --primary-HS: 0, 0%;
        --primary-HSL: 0, 0%, 99%;
        --secondary-color: #fcfcfc;
        --secondary-HS: 0, 0%;
        --secondary-HSL: 0, 0%, 99%;
        --accent-color: #9edaff;
        --accent-HS: 203, 100%;
        --accent-HSL: 203, 100%, 81%;
        --accent-2-color: #005fb8;
        --accent-2-HS: 209, 100%;
        --accent-2-HSL: 209, 100%, 36%;
        --error-color: #ff3c32;
        --error-HS: 3, 100%;
        --error-HSL: 3, 100%, 60%;
        --warning-color: #fba905;
        --warning-HS: 40, 97%;
        --warning-HSL: 40, 97%, 50%;
        --success-color: #00bf38;
        --success-HS: 138, 100%;
        --success-HSL: 138, 100%, 37%;
        --info-color: #00b5c2;
        --info-HS: 184, 100%;
        --info-HSL: 184, 100%, 38%;
      }
      `
    }
  }
}

import mediaQuery from '../utils/mediaQuerys'
export default window.header = function () {
  return {
    show: true,
    backgroundState: false,
    last_known_scroll_position: 0,
    ticking: false,
    open () {
      this.show = !this.show
    },
    close () { this.show = false },
    isOpen () { return this.show === true },
    menuResponsive () {
      mediaQuery('(min-width: 1024px)', (mql) => {
        if (mql.matches) {
          this.show = true
          this.backgroundState = false
        } else {
          this.show = false
        }
      })
    },
    mounted () {
      this.menuResponsive()
      window.addEventListener('scroll', () => {
        if (this.show === true) {
          this.menuResponsive()
        }
      })
      this.$watch('show', (value) => {
        if (this.show) {
          this.backgroundState = true
        } else {
          this.backgroundState = false
        }
      })
    }
  }
}

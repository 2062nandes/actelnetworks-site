import headerComponent from './headerComponent'
import themingComponent from './themingComponent'
import initialParticles from './initialParticles'
import contactFormComponent from './contactFormComponent'
// import tinySliderComponent from './tinySliderComponent'
export default {
  headerComponent,
  themingComponent,
  initialParticles,
  contactFormComponent,
  // tinySliderComponent
}

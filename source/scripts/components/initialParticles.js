export default window.initialParticles = function () {
  return {
    optionsParticles: {
      particles: {
        number: { 'value': 150 },
        'opacity': {
          'value': 0.5,
          'anim': {
            'enable': true,
            'speed': 1
          }
        },
        'size': {
          'value': 3,
          'random': true
        },
        'line_linked': { 'enable': false },
        'move': {
          'enable': true,
          'speed': 10,
          'direction': 'top',
          'random': true,
          'straight': true
        }
      },
      'interactivity': {
        'events': {
          'onhover': { 'enable': false },
          'onclick': { 'enable': false }
        }
      }
    },
    mounted (id) {
      window.particlesJS(id, this.optionsParticles)
    }
  }
}

import axios from 'axios'
export default window.contactForm = function () {
  return {
    contact: {
      name: null,
      email: null,
      message: null,
      response: null
    },
    alertMessage: false,
    async submitForm () {
      try {
        if (this.contact.name && this.contact.email && this.contact.message) {
          this.alertMessage = false;
          const resp = await axios.post('../../mail.php', { contact: this.contact })
          this.contact = await {
            name: null,
            email: null,
            message: null,
            response: resp.data
          }
          setTimeout(() => {
            this.contact.response = null
          }, 5000)
        } else {
          this.alertMessage = true;
        }
      } catch (error) {
        // console.error(error)
      }
    }
  }
}

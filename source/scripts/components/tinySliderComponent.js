// import * from 'tiny-slider/src/tiny-slider'
const { tns } = require('tiny-slider/src/tiny-slider')
export default window.tinySlider = function () {
  return {
    optionSlider: {
      container: '.slider-blog',
      controlsText: [
        '<i class="icon-left"></i>',
        '<i class="icon-right"></i>'
      ],
      autoplayText: [
        '<i class="icon-play"></i>',
        '<i class="icon-pause"></i>'
      ],
      speed: 2000,
      autoplay: true,
      controlsContainer: document.querySelector('.my-controls')
    },
    mounted () {
      tns(this.optionSlider)
    }
  }
}
